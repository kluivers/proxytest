//
//  main.m
//  ProxyTest
//
//  Created by Joris Kluivers on 3/26/12.
//  Copyright (c) 2012 Cardcloud. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JKAppDelegate.h"

@interface JKObject : NSObject
@property(nonatomic, weak) id delegate;
@end

@implementation JKObject
@synthesize delegate;
@end

@interface JKDelegateProxy : NSProxy
- (id) initWithObject:(NSObject *)object;
@end

@implementation JKDelegateProxy {
	NSObject *_obj;
}

- (id) initWithObject:(NSObject *)object {
	_obj = object;
	return self;
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {
    return [_obj methodSignatureForSelector:aSelector];
}

- (void)forwardInvocation:(NSInvocation *)anInvocation {
    [anInvocation setTarget:_obj];
    [anInvocation invoke];
    return;
}

@end




int main(int argc, char *argv[]) {
	@autoreleasepool {
	    JKObject *object = [[JKObject alloc] init];
		JKDelegateProxy *delegate = [[JKDelegateProxy alloc] initWithObject:@"Joris Kluivers"];
		object.delegate = delegate;
		NSLog(@"delegate: %@", delegate);
		NSLog(@"object.delegate: %@", object.delegate);
		
		return UIApplicationMain(argc, argv, nil, NSStringFromClass([JKAppDelegate class]));
	}
}
