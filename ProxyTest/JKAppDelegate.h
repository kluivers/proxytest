//
//  JKAppDelegate.h
//  ProxyTest
//
//  Created by Joris Kluivers on 3/26/12.
//  Copyright (c) 2012 Cardcloud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
